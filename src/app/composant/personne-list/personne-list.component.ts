import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Personne } from '../../classe/personne';
import { PersonneService } from '../../service/personne.service';

@Component({
  selector: 'app-personne-list',
  templateUrl: './personne-list.component.html',
  styleUrls: ['./personne-list.component.css']
})
export class PersonneListComponent implements OnInit {

  personnes: Observable<Personne[]>;
  constructor(private router: Router, private personneService: PersonneService) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData() {
    this.personnes = this.personneService.getPersonnesList();
    }

  deletePersonne(num: number) {
    this.personneService.deletePersonne(num)
    .subscribe(data => {
      console.log(data);
      this.reloadData();
    },
      error => console.log(error));
  }
  personneDetails(num: number) {
    this.router.navigate(['details', num]);
  }
  updatePersonne(num: number) {
    this.router.navigate(['update', num]);
  }

}
