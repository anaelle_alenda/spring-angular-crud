import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../../service/personne.service';
import { Personne } from '../../classe/personne';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-personne',
  templateUrl: './update-personne.component.html',
  styleUrls: ['./update-personne.component.css']
})
export class UpdatePersonneComponent implements OnInit {

  num: number;
  personne: Personne;
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router, private personneService: PersonneService) { }

  ngOnInit() {
    this.personne = new Personne();
    this.num = this.route.snapshot.params['num'];
    this.personneService.getPersonne(this.num).subscribe(data => {
      console.log(data)
      this.personne = data;
    }, error => console.log(error));
  }
  updatePersonne() {
    this.personneService.updatePersonne(this.num, this.personne).subscribe(data => {
      console.log(data)
    }, error => console.log(error));
    this.personne = new Personne();
    this.submitted = false;
    this.gotoList();
  }
  onSubmit() {
    this.submitted = true;
     this.updatePersonne();
    }
  gotoList() {
    this.router.navigate(['/personnes']);
  }

}
