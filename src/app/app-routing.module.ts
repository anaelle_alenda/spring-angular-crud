import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonneListComponent } from './composant/personne-list/personne-list.component';
import { PersonneDetailsComponent } from './composant/personne-details/personne-details.component';
import { CreatePersonneComponent} from './composant/create-personne/create-personne.component';
import { UpdatePersonneComponent} from './composant/update-personne/update-personne.component';

const routes: Routes = [
  { path: '', redirectTo: 'personne', pathMatch: 'full' },
  { path: 'personnes', component: PersonneListComponent },
  { path: 'add', component: CreatePersonneComponent },
  { path: 'update/:num', component: UpdatePersonneComponent },
  { path: 'details/:num', component: PersonneDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
