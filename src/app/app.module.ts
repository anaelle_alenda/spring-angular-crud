import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatePersonneComponent } from './composant/create-personne/create-personne.component';
import { PersonneDetailsComponent } from './composant/personne-details/personne-details.component';
import { PersonneListComponent } from './composant/personne-list/personne-list.component';
import { UpdatePersonneComponent } from './composant/update-personne/update-personne.component';

@NgModule({
  declarations: [
    AppComponent,
    CreatePersonneComponent,
    PersonneDetailsComponent,
    PersonneListComponent,
    UpdatePersonneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
